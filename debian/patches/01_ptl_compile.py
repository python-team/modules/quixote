From 0a1c209aaf2dd1cae54b221f287c7c96dd5f5249 Mon Sep 17 00:00:00 2001
From: SVN-Git Migration <python-modules-team@lists.alioth.debian.org>
Date: Thu, 8 Oct 2015 13:40:06 -0700
Subject: 01_ptl_compile.py

Patch-Name: 01_ptl_compile.py
---
 quixote/ptl/ptl_compile.py | 2 +-
 1 file changed, 1 insertion(+), 1 deletion(-)

diff --git a/quixote/ptl/ptl_compile.py b/quixote/ptl/ptl_compile.py
index dcf7a2e..087eddb 100644
--- a/quixote/ptl/ptl_compile.py
+++ b/quixote/ptl/ptl_compile.py
@@ -1,4 +1,4 @@
-#!/www/python/bin/python
+#!/usr/bin/env python
 """Compile a PTL template.
 
 First template function names are mangled, noting the template type.
