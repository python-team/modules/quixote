Source: quixote
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Neil Schemenauer <nas@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 python-all,
 python-all-dev,
 python-setuptools,
Standards-Version: 4.2.1
Rules-Requires-Root: no
Homepage: http://www.mems-exchange.org/software/quixote/
Vcs-Git: https://salsa.debian.org/python-team/packages/quixote.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/quixote

Package: python-quixote
Architecture: any
Depends:
 ${misc:Depends},
 ${python:Depends},
 ${shlibs:Depends},
Suggests:
 python-quixote-doc,
Description: Highly Pythonic Web application framework
 Quixote is yet another framework for developing Web applications in
 Python. The design goals were:
 .
  1) To allow easy development of Web applications where the
     emphasis is more on complicated programming logic than
     complicated templating.
 .
  2) To make the templating language as similar to Python as possible,
     in both syntax and semantics.  The aim is to make as many of the
     skills and structural techniques used in writing regular Python
     code applicable to Web applications built using Quixote.
 .
  3) No magic.  When it's not obvious what to do in
     a certain case, Quixote refuses to guess.
 .
 If you view a web site as a program, and web pages as subroutines,
 Quixote just might be the tool for you.  If you view a web site as a
 graphic design showcase, and each web page as an individual work of
 art, Quixote is probably not what you're looking for.

Package: python-quixote-doc
Section: doc
Depends:
 ${misc:Depends},
Architecture: all
Description: Quixote web application framework documentation
 This package contains the documentation and examples for Quixote.
 Quixote is yet another framework for developing Web applications in
 Python. If you view a web site as a program, and web pages as
 subroutines, Quixote just might be the tool for you. If you view a
 web site as a graphic design showcase, and each web page as an
 individual work of art, Quixote is probably not what you're looking for.
 The full list of its design goals is listed in the description to
 python-quixote package.
